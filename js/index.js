$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({ interval: 1500});
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('El modal se está mostrando');

        $('#btnContacto').removeClass('btn-outline-success');
        $('#btnContacto').addClass('btn-primary');
        $('#btnContacto').prop('disabled', true);

    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('El modal se mostro');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('El modal se está ocultando');
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('El modal se ocultó');

        $('#btnContacto').removeClass('btn-primary');
        $('#btnContacto').addClass('btn-outline-success');
        $('#btnContacto').prop('disabled', false);

    });
});
